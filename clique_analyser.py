import networkx as nx
import pandas as pd


def extract_cliques_as_list(graph):
    clique_set = set()
    all_cliques = list(nx.find_cliques(graph))
    for clique in all_cliques:
        clique_set.add(','.join(str(ele) for ele in clique))

    clique_lists = []
    for single_clique_string in clique_set:
        single_clique = single_clique_string.split(',')
        clique_lists.append(single_clique)

    return clique_lists


def generate_cliques_data_frame(clique_list, graph, attribute_to_category_dict):
    column_names = ['Clique', 'Categories', 'Number of edges', 'total edge weight', 'mean weight']
    clique_data_frame = pd.DataFrame(columns=column_names)
    for clique in clique_list:
        cats = set()
        edge_counter = 0
        total_edge_weight = 0
        for node in clique:
            cats.add(attribute_to_category_dict.get(node))
        for idx in range(len(clique) - 1):
            for in_idx in range(idx+1, len(clique)):
                edge = graph.get_edge_data(clique[idx], clique[in_idx])
                if edge:
                    edge_counter += 1
                    total_edge_weight += edge.get('weight')

        mean = total_edge_weight / edge_counter if edge_counter > 0 else 0.0
        clique_data_frame = clique_data_frame.append({
            'Clique': clique,
            'Categories': sorted(cats, key=str.lower),
            'Number of edges': edge_counter,
            'total edge weight': total_edge_weight,
            'mean weight': mean},
            ignore_index=True)

    return clique_data_frame
